# UnityFlexML

<div align="center">
  <img width="800" src="Image_SimulationToReal.png" alt="unityflexml">
</div>
A simulation environment providing support for PBD soft bodies simulation within Unity3D is used to train a Reinforcement Learning (RL) agent to manipulate soft tissues. 


## How to Run

To run this demo you need to have Unity-64bit (version 2019.3.0 or later) installed on your system. 
Afterwards, you have to:

1. Clone or download this repo.
2. Start Unity.
3. Select 'Open Project' and select the root folder that you have just cloned.
4. Press 'Play' button to run a sample trajectory.

This scene has been tested with the following configuration: 
- Windows 10 64-bit, Visual Studio 2017.


## Video
[![Video](https://www.youtube.com/embed/kB82_3v9P3I/0.jpg)](https://www.youtube.com/embed/kB82_3v9P3I "UnityFlexML")


## References
Tagliabue, E., Pore, A., Dall’Alba, D., et al. ["Soft Tissue Simulation Environment to Learn Manipulation Tasks in Autonomous Robotic Surgery"](https://atlas-itn.eu/wp-content/uploads/2020/11/Amey1.pdf). IROS (2020).

Altair Robotics Lab -
University of Verona

﻿using DVRK;
using MLAgents;
using UnityEngine;
using UnityEngine.Serialization;
using System;
using System.Reflection;
using NVIDIA.Flex;
using System.Collections.Generic;
//using UnityEditor.Experimental.GraphView;
using Random = System.Random;

public class AgentScript : Agent
{

    /// Point _ee start position
    [SerializeField] private test_collision shadow;
    /// Point _ee start position
    [FormerlySerializedAs("startPosRot")] [SerializeField] private GameObject ee;
    /// Left claw of the grippe 
    [SerializeField] private URDFJoint leftGripper;
    /// Right claw of the gripper
    [SerializeField] private URDFJoint rightGripper;
    /// Academy's agent 
    [SerializeField] private DvrkAcademy dvrkAcademy;
    /// Initial position of the ee in order to reset the gripper position   
    private Vector3 _resetPosition;
    /// Initial rotation of the ee in order to reset the gripper rotation   
    private Quaternion _resetRotation;
    /// Multiplicative constant for a correct movement of the clamp's position
    private float _deltaMovement;
    /// Multiplicative constant for a correct movement of the clamp's rotation
    private float _deltaRotation;
    /// Boolean for enable scene limits
    private bool _enableSceneLmits;
    /// Max limit for pegs and rings plane 
    private GameObject _limitMax;
    /// Min limit for pegs and rings plane 
    private GameObject _limitMin;
    /// Array containing all rings in the scene
    //[SerializeField] private AttachToroid_Trigger[] _rings;
    private GameObject _gettoppoint;
    /// Array containing all pegs in the scene
    private GameObject[] _pegs;

    /// Actual ee position and rotation
    private GameObject[] _fat;

    private Transform _eePositionRotation;
    /// Boolean for when a ring exit from the platform
    //private bool _aRingHasFallen;

    /// Backup Rotation of EE
    private Vector3 _beeRotation;
    /// Backup Position of EE used for the shadow
    private Vector3 _beePosition;
    ///Backup Backup Rotation of EE used for the shadow
    private Vector3 _bbeeRotation;
    ///Backup Backup Position of EE used for the shadow
    private Vector3 _bbeePosition;

    /// Index which represents the ring that the agent has to take
    //private int _ringToTake;
    /// Boolean that represents if the agent is hitting a pole
    /// Boolean that represents if the agent has already completed the ring
    /// It's used to give the rewards to the agent 
    private bool[] _alreadyCompleted;

    private int step;

    private bool isGrippin = false;
    private Vector3 Kidney_reset;

    private Vector3 fat_reset_position;

    private Quaternion fat_reset_rotation;

    public override void InitializeAgent()

    {
        _alreadyCompleted = new bool[1];
        _resetPosition = ee.transform.position;
        _resetRotation = ee.transform.rotation;

        _beeRotation = _beePosition = Vector3.zero;

        _deltaMovement = dvrkAcademy.deltaMovement;
        _deltaRotation = dvrkAcademy.deltaRotation;
        _limitMax = dvrkAcademy.GetLiminitMax();
        _limitMin = dvrkAcademy.GetLiminitMin();
        _gettoppoint = dvrkAcademy.Gettoppoint();
        _enableSceneLmits = dvrkAcademy.GetEnableSceneLimits();
        _pegs = dvrkAcademy.GetPegs();
        _fat = dvrkAcademy.Getfat();
        _eePositionRotation = ee.transform;
        step = 0;
        Kidney_reset = GameObject.Find("Lesion").transform.position;
        fat_reset_position = _fat[0].transform.position;
        //fat_reset_position = GameObject.Find("Fat").transform.position;
        //fat_reset_rotation = GameObject.Find("Fat").transform.rotation;
        fat_reset_rotation = _fat[0].transform.rotation;
    }

    public override void CollectObservations()
    {
        AddVectorObs(dvrkAcademy.GetObservation());
    }

    public override void AgentAction(float[] vectorAction, string textAction)
    {

        if (step < 15)
        {
            //FlexActor.relGrasp = true;
            GameObject.Find("Lesion").transform.position = Kidney_reset;
            //GameObject.Find("Fat").transform.position = fat_reset ;
            //GameObject.Find("Cyl_Red (1)").transform.position = pegs_reset + pegs_reset_offset;
        }

        step++;

        CalculateRewards();
        for (int i = 0; i < 3; i++)
            vectorAction[i] = vectorAction[i] == 2 ? -1 : vectorAction[i];

        //if (step < 20)
        //{
        ///Vector3 position = new Vector3( 0,0,0);
        //Vector3 rotation =new Vector3( 0,0,0);
        //}
        //else
        //{
        //Vector3 position = new Vector3(vectorAction[0]*_deltaMovement,vectorAction[1]*_deltaMovement,vectorAction[2]*_deltaMovement);
        //Vector3 rotation =new Vector3( 0,0,0);
        //}
        Vector3 position = new Vector3(vectorAction[0] * _deltaMovement, vectorAction[1] * _deltaMovement, vectorAction[2] * _deltaMovement);
        Vector3 rotation = new Vector3(0, 0, 0);
        //float nipperValue = 0;

        // If the previous move was legit
        if (dvrkAcademy.GetPreventCollision())
        {
            PreventCollision(position, rotation);
        }
        else
        {
            SetAgentPosition(position, rotation);
        }

        if (AmIDone())
        {

            //AgentReset();
            //AgentOnDone()
            //Debug.Log("I am done");
            Debug.Log("I AM DONE AT Iteration: " + count.ToString() + "\nBUT NOT RESETTING");
            Done();
        }


    }

    private bool AmIDone()
    {

        if (FlexActor.activeGrab == true)
            return (Vector3.Distance(_gettoppoint.transform.position, ee.transform.position) < 0.2);

        return false;
    }

    
    public int[] saveGreen = new int[100];

    int count = 0;
    public override void AgentReset()
    {
        
        if (step == 0)
            FlexActor.relGrasp = false;
        else
            FlexActor.relGrasp = true;

        FlexActor.resFatPos = true;

        ee.transform.position = _resetPosition;
        //ee.transform.position = _resetPosition;
        ee.transform.rotation = _resetRotation;
        _beePosition = new Vector3(0, 0, 0);
        _beeRotation = new Vector3(0, 0, 0);
        _alreadyCompleted = new bool[1];

        shadow.Start();

        //_fat[0].transform.position = fat_reset_position;
        //_fat[0].transform.rotation = fat_reset_rotation;
        count++;

    }

    //private void OpenAgentNipper(float trigger)
    //{
    //if (CompareTag("PSM2") && !AttachToroid_Trigger.JointEnablePsm2)
    //{
    //Add trigger's angle for angle of nipper
    //leftGripper.SetJointValue( 20 * trigger);
    //rightGripper.SetJointValue( -20 * trigger);
    //}
    //}

    /// <summary>
    /// Function used to set the agent position in the environment 
    /// </summary>
    /// <param name="pos"> Desired position </param>
    /// <param name="rotation"> Desired direction </param>
    private void SetAgentPosition(Vector3 pos, Vector3 rotation)
    {
        var tmp = _eePositionRotation.transform.transform.eulerAngles.x + rotation.x;
        rotation.x = tmp > 360 ? tmp - 360 : tmp;
        tmp = _eePositionRotation.transform.transform.eulerAngles.y + rotation.y;
        rotation.y = tmp > 360 ? tmp - 360 : tmp;
        tmp = _eePositionRotation.transform.transform.eulerAngles.z + rotation.z;
        rotation.z = tmp > 360 ? tmp - 360 : tmp;
        _eePositionRotation.transform.transform.eulerAngles = rotation;

        //Controls of the limits in X Y Z
        if (_enableSceneLmits)
        {
            Vector3 desired = ee.transform.position + pos;
            if (desired.x > _limitMax.transform.position.x || desired.x < _limitMin.transform.position.x)
                pos.x = 0f;
            if (desired.y > _limitMax.transform.position.y || desired.y < _limitMin.transform.position.y)
                pos.y = 0f;
            if (desired.z > _limitMax.transform.position.z || desired.z < _limitMin.transform.position.z)
                pos.z = 0f;
        }

        ee.transform.position += pos;

    }

    /// <summary>
    /// Function used to add the reward.
    /// It's called each step 
    /// </summary>
    private void CalculateRewards()
    {
        float reward;
        float maxDistance = Vector3.Distance(_limitMax.transform.position, _limitMin.transform.position);
        //Debug.Log("maxdistance: "+maxDistance);
        if (FlexActor.activeGrab == true)
        {

            //reward = (Vector3.Distance(_gettoppoint.transform.position, ee.transform.position) * -0.5f / maxDistance);
            if (Vector3.Distance(_gettoppoint.transform.position, ee.transform.position) > 0.51f)
                reward = -0.7f;
            else
            {
                //reward = (Vector3.Distance(_gettoppoint.transform.position, ee.transform.position) * -0.5f / maxDistance) - 0.3f;
                reward = 0.5f - Vector3.Distance(_gettoppoint.transform.position, ee.transform.position);
            }
            //reward = (ee.transform.position.y - 0.515f) - 0.5f; ;
            //Debug.Log("reward: " + reward);
        }
        else
        {

            reward = (Vector3.Distance(_pegs[0].transform.position, ee.transform.position) * -0.5f / maxDistance) - .5f;
            //Debug.Log("reward: " + reward);
            //reward =  (Vector3.Distance(ee.transform.position, _rings[0].transform.position)*-0.3956f)-.5f;
        }
        //Debug.Log("reward: "+reward);
        AddReward(reward);

    }


    private void PreventCollision(Vector3 position, Vector3 rotation)
    {
        if (shadow.MoveTo(position, rotation))
        {
            SetAgentPosition(_bbeePosition, _bbeeRotation); //move ..
            _bbeeRotation = _beeRotation;
            _bbeePosition = _beePosition;
            _beePosition = position; //..and then save the possible next one move
            _beeRotation = rotation;
        }
        else
        {
            _bbeeRotation = _beePosition = _beeRotation = _bbeePosition = new Vector3(0f, 0f, 0f);
            SetAgentPosition(_bbeePosition, _bbeeRotation); //move
        }
    }

}
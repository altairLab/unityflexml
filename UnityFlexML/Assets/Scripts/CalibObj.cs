﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Globalization;

public class CalibObj : MonoBehaviour
{
    // Start is called before the first frame update
    CultureInfo culture = CultureInfo.CreateSpecificCulture("en-GB");
    NumberStyles style = NumberStyles.Float;

    Vector3 traslation;
    Quaternion rotation_quaternion;
    Matrix4x4 rotTrasl;

    void Start()
    {

        var sr = new StreamReader(this.name +".txt");
        string par = sr.ReadLine();
        sr.Close();
        string[] par_array = par.Split('\t');

        traslation.x = float.Parse(par_array[0], style, culture);
        traslation.y = float.Parse(par_array[1], style, culture);
        traslation.z = float.Parse(par_array[2], style, culture);

        rotation_quaternion.x = float.Parse(par_array[3], style, culture);
        rotation_quaternion.y = float.Parse(par_array[4], style, culture);
        rotation_quaternion.z = float.Parse(par_array[5], style, culture);
        rotation_quaternion.w = float.Parse(par_array[6], style, culture);

        traslation = traslation * 10;
        rotTrasl.SetTRS(traslation, rotation_quaternion, Vector3.one);

        rotTrasl= Matrix4x4.Rotate(Quaternion.Euler(new Vector3(90, 0, 0))) * rotTrasl;
        rotTrasl= Matrix4x4.Rotate(Quaternion.Euler(new Vector3(0, 90, 0))) * rotTrasl;

        Vector3 translation = rotTrasl.GetColumn(3);
        this.transform.position = new Vector3(translation.x, -translation.y, translation.z);

        Vector3 eulerAngles = GetRotation(rotTrasl).eulerAngles;
        this.transform.rotation = Quaternion.Euler(-eulerAngles.x, eulerAngles.y, -eulerAngles.z);

        this.transform.Rotate(90, 0, 0, Space.Self);
    }

    // Update is called once per frame
    private Quaternion GetRotation(Matrix4x4 matrix)
    {
        Quaternion q = new Quaternion();
        q.w = Mathf.Sqrt(Mathf.Max(0, 1 + matrix.m00 + matrix.m11 + matrix.m22)) / 2;
        q.x = Mathf.Sqrt(Mathf.Max(0, 1 + matrix.m00 - matrix.m11 - matrix.m22)) / 2;
        q.y = Mathf.Sqrt(Mathf.Max(0, 1 - matrix.m00 + matrix.m11 - matrix.m22)) / 2;
        q.z = Mathf.Sqrt(Mathf.Max(0, 1 - matrix.m00 - matrix.m11 + matrix.m22)) / 2;
        q.x = _copysign(q.x, matrix.m21 - matrix.m12);
        q.y = _copysign(q.y, matrix.m02 - matrix.m20);
        q.z = _copysign(q.z, matrix.m10 - matrix.m01);
        return q;
    }

    private float _copysign(float sizeval, float signval)
    {
        return Mathf.Sign(signval) == 1 ? Mathf.Abs(sizeval) : -Mathf.Abs(sizeval);
    }
}

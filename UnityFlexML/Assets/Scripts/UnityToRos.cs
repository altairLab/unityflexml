﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;

using DVRK;
using NVIDIA.Flex;
// [System.Serializable]
// public class Packet {
// 	public EE_State psm1_ee;
// 	public EE_State psm2_ee;
// 	public Packet(EE_State psm1_ee,EE_State psm2_ee) {
// 		this.psm1_ee = psm1_ee;
// 		this.psm2_ee = psm2_ee;
// 	}
// }

[System.Serializable]
public class EE_State{
	public Vector3 xyz;
	public Quaternion RPY;
	public bool gripper;
	public EE_State(Vector3 xyz, Quaternion RPY, bool gripper) {
		this.xyz = xyz;
		this.RPY = RPY;
		this.gripper = gripper;
	}

    public override bool Equals(object obj) {
        return Equals(obj as EE_State);
    }

    public bool Equals(EE_State other) {
        return other != null && other.xyz.Equals(this.xyz) && other.RPY.Equals(this.RPY) && other.gripper.Equals(this.gripper);
    }
}
public class UnityToRos : MonoBehaviour
{

	// public DVRK.PSM psm1;
	public FlexActor actor;
	public GameObject cube;
	// public DVRK.PSM psm2;
	private EE_State psm1_state;
	// private EE_State psm2_state;
	// private Packet packet;
	Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
	public int port = 47051;
	public int periodInMillis = 20;
	[SerializeField] private string IP;
    IPAddress serverAddr;


    IPEndPoint endPoint;
	byte[] buffer;

    private EE_State lastSent;
    private int counter;
    string msg_to_send;

    void Start() {
        serverAddr = IPAddress.Parse(IP);
    }

    private void FixedUpdate() {
        psm1_state = new EE_State(Unity2Ros(UnityToRosXZ(cube.transform.position), 0.1f), Unity2Ros(cube.transform.rotation), FlexActor.activeGrab);

        if (counter == 0) {
            msg_to_send = JsonUtility.ToJson(psm1_state);
            Debug.Log(msg_to_send);
            SendData(msg_to_send);
            lastSent = psm1_state;
        } else if (!lastSent.Equals(psm1_state)) {
            msg_to_send = JsonUtility.ToJson(psm1_state);
           // Debug.Log(msg_to_send);
            SendData(msg_to_send);
            lastSent = psm1_state;
        } else {
            Debug.Log("Object did not move from last update.");
        }
    }

	void SendData(string msg) {
		endPoint = new IPEndPoint(serverAddr, port);
		buffer = Encoding.ASCII.GetBytes(msg);
		socket.SendTo(buffer , endPoint);
        counter++;
	}

	public Quaternion Unity2Ros(Quaternion quaternion) {
		return new Quaternion(-quaternion.z, quaternion.x, -quaternion.y, quaternion.w);
	}

	public Vector3 Unity2Ros(Vector3 vector3, float scale) {
		return new Vector3(vector3.z * scale, -vector3.x * scale, vector3.y * scale);
	}

    private Vector3 UnityToRosXZ(Vector3 transform)
    {
        return new Vector3(-transform.x,transform.y,-transform.z);
    }
}